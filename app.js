var express = require("express");
var app = express();
var request = require("request");

app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));

app.get("/", function(req, res){
   res.render("search");
});

app.get("/results", function(req, res){
    var query = req.query.search;
    var url = "https://api.openaq.org/v1/latest/?city=" + query;
    if(query === "Warszawa") {
        url = "https://api.openaq.org/v1/latest/?location=Komunikacyjna";
    }
    if(query === "Płock") {
        url = "https://api.openaq.org/v1/latest/?location=Reja";
    }
    request(url, function(error, response, body){
        if(!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            data["results"][0]["measurements"].forEach(function(measure) {
                if(measure["parameter"] === "pm25") {
                    var liczba_papierosow = measure["value"]/22;
                    var date = measure["lastUpdated"].substring(0, 10);
                    liczba_papierosow = Math.round(liczba_papierosow);
                    res.render("results", {liczba_papierosow: liczba_papierosow, data: date});
                }
            });
        }
    });
});



app.listen(process.env.PORT, process.env.IP, function(){
    console.log("App has started!!!");
});
